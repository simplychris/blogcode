﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Domain.Interfaces;
using NUnit.Framework;
using StructureMap;

namespace AdapterPluginExample1.Tests
{
    [TestFixture]
    public class StructureMapTests
    {
        [TestFixtureSetUp]
        public void Setup ()
        {            
            
        }

        [Test]
        public void should_load_null_action_type ()
        {
            string assemblyName = "AdapterPluginExample1.Domain";

            var assembly = Assembly.Load(assemblyName);

            ObjectFactory.Initialize(x =>
                                         {
                                             
                                         });
            ObjectFactory.Configure(config => config.Scan(scan =>
                                                               {                                                                   
                                                                   scan.AddAllTypesOf<IExampleAction>();
                                                                   scan.Assembly(assemblyName);
                                                                   scan.WithDefaultConventions();
                                                               }
                                                   ));
            
            var registry = ObjectFactory.WhatDoIHave();            
            var instances = ObjectFactory.GetAllInstances<IExampleAction>();
            Assert.That(instances.Count, Is.GreaterThan(0));
        }
    }
}
