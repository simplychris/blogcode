﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AdapterPluginExample1.Domain.IoC;
using Domain.Interfaces;


namespace AdapterExample1.Forms.Controls
{
    public partial class RegisteredPlugins : UserControl
    {
        private IList<IExampleAction> _actionList { get; set; }
       

        public RegisteredPlugins ()
        {
            InitializeComponent();
            _actionList = new List<IExampleAction>();

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void RegisteredPlugins_Load(object sender, EventArgs e)
        {
            
        }

        private void btn_Register_Click(object sender, EventArgs e)
        {
            var assemblyName = GetAssemblyName();
            if (String.IsNullOrEmpty(assemblyName))
                return;

            var fileName = new FileInfo(assemblyName).Name;
            var baseName = fileName.Replace(".dll", "");

            
            if (!IoCContainer.ContainsType<IExampleAction>(baseName))
            {
                MessageBox.Show("Assembly does not contain concrete IExampleAction implementation(s).");
                return;
            }

            var typelist = IoCContainer.GetTypesFromAssembly<IExampleAction>(baseName);

            foreach (var type in typelist)
            {
                listBox1.Items.Add(type.Name);
            }
            var instanceList = IoCContainer.GetAllInstancesFromAssembly<IExampleAction>(baseName);
            
            foreach (var exampleAction in instanceList)
            {
                _actionList.Add(exampleAction);                           
            }
        }


        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        public IList<IExampleAction> GetActionList()
        {
            IList<IExampleAction> pipelineActions = new List<IExampleAction>();
            foreach (var textType in listBox1.Items)
            {
                var type = _actionList.Where(x => x.GetType().Name.ToString() == textType.ToString()).ToList()[0];
                pipelineActions.Add(type);
            }
            return pipelineActions;
        }

        private string GetAssemblyName()
        {
            openFileDialog1.Filter = "dll Assemblies | *.dll";
            openFileDialog1.ShowDialog();

            return (openFileDialog1.FileName);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var selectedIndex = listBox1.SelectedIndex;
            if (listBox1.SelectedIndex == 0)
                return;           

           listBox1.Items.Insert(listBox1.SelectedIndex-1, listBox1.SelectedItem);
           
           listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            listBox1.SelectedIndex = selectedIndex - 1;


        }

        private void button2_Click(object sender, EventArgs e)
        {
            var selectedIndex = listBox1.SelectedIndex;

            if (listBox1.SelectedIndex == listBox1.Items.Count - 1)
                return;
            
            listBox1.Items.Insert(listBox1.SelectedIndex + 2, listBox1.SelectedItem);
            listBox1.Items.RemoveAt (listBox1.SelectedIndex);

            listBox1.SelectedIndex = selectedIndex + 1;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listBox1.Items.Count == 0 || listBox1.SelectedIndex < 0)
                return;
            
            listBox1.Items.RemoveAt(listBox1.SelectedIndex);

        }

    }
}
