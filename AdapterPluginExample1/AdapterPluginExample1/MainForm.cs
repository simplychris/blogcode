﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AdapterPluginExample1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void executeActionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var actionList = registeredPlugins1.GetActionList();            

            var text = "This text was processed by a plugin";
            foreach (var action in actionList )
            {
                text = action.ProcessMessage(text);
            }
            textBox1.Text += text + System.Environment.NewLine;
        }

        private void registeredPlugins1_Load(object sender, EventArgs e)
        {

        }
    }
}
