﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Interfaces;

namespace AdapterPluginExample1.Adapters
{
    public class StarSurrounder : IExampleAction
    {
        public string GetPluginName()
        {
            throw new NotImplementedException();
        }

        public string ProcessMessage(string message)
        {
            return string.Format("*{0}*", string.Format(message));
        }
    }
}
