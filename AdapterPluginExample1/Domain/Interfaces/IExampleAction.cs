﻿namespace Domain.Interfaces
{
    public interface IExampleAction
    {
        string GetPluginName();
        string ProcessMessage(string message);
    }
}