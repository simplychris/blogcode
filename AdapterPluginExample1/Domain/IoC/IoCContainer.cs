﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using StructureMap;

namespace AdapterPluginExample1.Domain.IoC
{
    public class IoCContainer
    {
        public static IContainer Container { get; set; }   


        public IoCContainer ()
        {
            Container = new Container();
        }

        public static void Configure ()
        {
            //void scanner = new 
            Container.Configure(config => config.Scan(scan =>
                                                          {
                                                              scan.LookForRegistries();
                                                              scan.WithDefaultConventions();
                                                          }));
        }

        public static T GetInstance <T> ()
        {
            return Container.GetInstance<T>();     
        }

        public static IList<T> GetAllInstances <T> ()
        {
            return Container.GetAllInstances<T>();
        }
        public static IList<T> GetAllInstancesFromAssembly <T> (string assemblyName)
        {
            var container = new Container();
            ConfigureContainer<T>(container, assemblyName);


            return container.GetAllInstances<T>();
        }

        
        public static bool ContainsType<T>(string assemblyName)
        {
           
            var container = new Container();


            ConfigureContainer<T>(container, assemblyName);
            
            IList <T> instance;
            try
            {
                instance = container.GetAllInstances<T>();
            }
            catch (Exception)
            {

                return false;
            }
            if (instance == null || instance.Count == 0)
                return false;

            return true;
        }


        public static IList<Type> GetTypesFromAssembly <T> (string assemblyName)
         {
            var container = new Container();
            ConfigureContainer<T>(container, assemblyName);

            var instances = container.GetAllInstances<T>();
            var typelist = new List<Type>();
            foreach (var instance in instances)
            {
                typelist.Add(instance.GetType());
            }
            return typelist;

        }

        public static void ConfigureContainer <T> (IContainer container, string assemblyName)
        {            
            container.Configure((config => config.Scan(scan =>
                                                           {

                                                               scan.Assembly(Assembly.Load(assemblyName));
                                                               scan.AddAllTypesOf<T>();                                                                                                                          
                                                           })));
        }
    }
}