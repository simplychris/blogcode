﻿using Domain.Interfaces;

namespace AdapterPluginExample1.Domain
{
    public class BracketAction : IExampleAction 
    {
        public string GetPluginName()
        {
            return "BracketAction";
        }

        public string ProcessMessage(string message)
        {
            return string.Format("[{0}]", message);
        }
    }
}