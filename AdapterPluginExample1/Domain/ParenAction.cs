﻿using Domain.Interfaces;
using StructureMap;

namespace AdapterPluginExample1.Domain
{
    public class ParenAction : IExampleAction 
    {

        [DefaultConstructor]
        public ParenAction ()
        {
            
        }

        public string GetPluginName()
        {
            return "Paren Plugin";
        }

        public string ProcessMessage(string message)
        {
            return string.Format("({0})", message);
        }
    }
}
